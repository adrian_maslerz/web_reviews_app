import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CalendarModule } from 'primeng/primeng';

import { AppRoutingModule } from './app-routing.module';

//components
import { AppComponent } from './app.component';
import { EtlComponent } from './resources/pages/etl/etl.component';
import { DatabaseComponent } from './resources/pages/database/database.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ProductsListComponent } from './resources/pages/database/products-list/products-list.component';
import { EditProductComponent } from './resources/pages/database/edit-product/edit-product.component';
import { ReviewsListComponent } from './resources/pages/database/reviews-list/reviews-list.component';
import { EditReviewComponent } from './resources/pages/database/edit-review/edit-review.component';
import { PaginationComponent } from './resources/partials/pagination/pagination.component';

@NgModule({
    declarations: [
        AppComponent,
        EtlComponent,
        DatabaseComponent,
        ProductsListComponent,
        EditProductComponent,
        ReviewsListComponent,
        EditReviewComponent,
        PaginationComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        HttpClientModule,
        ReactiveFormsModule,
        CalendarModule
    ],
    providers: [],
    bootstrap: [ AppComponent ]
})
export class AppModule {}
