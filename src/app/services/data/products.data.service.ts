import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';
import { Observable } from 'rxjs/index';
import { map } from 'rxjs/internal/operators';
import { PaginationInterface } from '../../interfaces/pagination.interface';
import { Product } from '../../models/product.model';
import { SuccessInterface } from '../../interfaces/success.interface';

@Injectable()
export class ProductsDataService
{

    constructor(private apiService: ApiService) { }

    getProducts(page: number, search: string, sort: string, dateFrom: number, dateTo: number, reviewsFrom: number, reviewsTo: number) : Observable<any>
    {
        return this.apiService.getProducts(page, search, sort, dateFrom, dateTo, reviewsFrom, reviewsTo)
            .pipe(map((data: PaginationInterface) => {
                data.results = data.results.map(product => new Product(product));
                return data;
            }));
    }

    getProduct(id: string) : Observable<any>
    {
        return this.apiService.getProduct(id).pipe(map((data: Product) => new Product(data)));
    }

    updateProduct(id: string, data: Object, file: File) : Observable<any>
    {
        const attributes = data["attributes"].map(attribute => {
            return {
                name: attribute.name,
                values: attribute.values.map(value => value.value)
            }
        });

        const formData = new FormData();
        formData.append("name", data["name"]);
        formData.append("attributes", JSON.stringify(attributes));
        if(file)
            formData.append("image", file);

        return this.apiService.updateProduct(id, formData).pipe(map((data: SuccessInterface) => data));
    }
}
