import { Injectable } from '@angular/core';
import { map } from 'rxjs/internal/operators';
import { Observable } from 'rxjs/index';
import { PaginationInterface } from '../../interfaces/pagination.interface';
import { ApiService } from '../api.service';
import { Review } from '../../models/review.model';
import { SuccessInterface } from '../../interfaces/success.interface';

@Injectable()
export class ReviewsDataService
{

    constructor(private apiService: ApiService) { }

    getProductReviews(id: string, page: number) : Observable<any>
    {
        return this.apiService.getProductReviews(id, page)
            .pipe(map((data: PaginationInterface) => {
                data.results = data.results.map(review => new Review(review));
                return data;
            }));
    }
    getReview(id: string) : Observable<any>
    {
        return this.apiService.getReview(id).pipe(map((data: Review) => new Review(data)));
    }

    updateReview(id: string, data: Object) : Observable<any>
    {
        return this.apiService.updateReview(id, data).pipe(map((data: SuccessInterface) => data));
    }
}
