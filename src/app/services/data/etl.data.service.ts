import { Injectable } from '@angular/core';
import { map } from 'rxjs/internal/operators';

import { ApiService } from '../api.service';
import { SuccessInterface } from '../../interfaces/success.interface';

@Injectable()
export class EtlDataService
{
    constructor(private apiService: ApiService) { }

    etl(search: string)
    {
        return this.apiService.etl(search).pipe(map((data: SuccessInterface) => data));
    }

    extract(search: string)
    {
        return this.apiService.extract(search).pipe(map((data: SuccessInterface) => data));
    }

    transform()
    {
        return this.apiService.transform().pipe(map((data: SuccessInterface) => data));
    }

    load()
    {
        return this.apiService.load().pipe(map((data: SuccessInterface) => data));
    }

    clearDatabase()
    {
        return this.apiService.clearDatabase().pipe(map((data: SuccessInterface) => data));
    }
}
