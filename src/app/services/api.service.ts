import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/index';

import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ApiService
{
    private url: string = environment.api + "/api";
    constructor(private http: HttpClient) { }

    //ETL
    etl(search: string) : Observable<any>
    {
        return this.http.post(this.url + "/etl", { search: search });
    }

    extract(search: string) : Observable<any>
    {
        return this.http.post(this.url + "/etl/extract", { search: search });
    }

    transform() : Observable<any>
    {
        return this.http.post(this.url + "/etl/transform", {});
    }

    load() : Observable<any>
    {
        return this.http.post(this.url + "/etl/load", {});
    }

    clearDatabase() : Observable<any>
    {
        return this.http.delete(this.url + "/etl/purge");
    }

    //products
    getProducts(page: number, search: string, sort: string, dateFrom: number, dateTo: number, reviewsFrom: number, reviewsTo: number) : Observable<any>
    {
        const params = new HttpParams()
            .set("search", search)
            .set("page", page.toString())
            .set("sort", sort)
            .set("from_date", dateFrom.toString())
            .set("to_date", dateTo.toString())
            .set("from_reviews", reviewsFrom.toString())
            .set("to_reviews", reviewsTo.toString())

        return this.http.get(this.url + "/products", { params: params });
    }

    getProduct(id: string) : Observable<any>
    {
        return this.http.get(this.url + "/products/" + id);
    }

    updateProduct(id: string, data: FormData) : Observable<any>
    {
        return this.http.put(this.url + "/products/" + id, data);
    }

    //reviews
    getProductReviews(id: string, page: number) : Observable<any>
    {
        const params = new HttpParams().set("page", page.toString())
        return this.http.get(this.url + "/products/" + id + "/reviews", { params: params });
    }

    getReview(id: string) : Observable<any>
    {
        return this.http.get(this.url + "/reviews/" + id);
    }

    updateReview(id: string, data: Object) : Observable<any>
    {
        return this.http.put(this.url + "/reviews/" + id, data);
    }
}
