import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs/index';
import * as io from "socket.io-client";
import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class WebSocketService
{
    private socket;
    constructor() { }

    connect() : Subject<MessageEvent>
    {
        this.socket = io(environment.api);

        let observable = new Observable(observer => {
            this.socket.on('message', (data) => {
                observer.next(data);
            })
            return () => {
                this.socket.disconnect();
            }
        });

        let observer = {
            next: (data: Object) => {
                this.socket.emit('message', JSON.stringify(data));
            },
        };

        return Subject.create(observer, observable);
    }
}
