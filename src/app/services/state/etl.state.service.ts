import { Injectable } from '@angular/core';
import { WebSocketService } from '../websocket.service';
import { Subject } from 'rxjs/index';
import { map } from 'rxjs/internal/operators';

@Injectable({
    providedIn: 'root'
})
export class EtlStateService
{
    public inProgress: boolean = false;

    public extract: boolean = false;
    public transform: boolean = false;
    public load: boolean = false;

    public logs: Array<{message: string, date: Date}> = [];

    constructor(private webSocketService: WebSocketService)
    {
        webSocketService.connect().pipe(map(data => {
            if(data["etl"])
            {
                this.inProgress = data["etl"]["inProgress"];
                this.extract = data["etl"]["extract"];
                this.transform = data["etl"]["transform"];
                this.load = data["etl"]["load"];
            }
            else if(data["log"])
                return data["log"];
        }))
        .subscribe((log: string) => {
            if(log)
                this.logs.push({ message: log, date: new Date() })
        });
    }
}
