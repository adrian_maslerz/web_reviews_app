//core
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

//components
import { EtlComponent } from './resources/pages/etl/etl.component';
import { DatabaseComponent } from './resources/pages/database/database.component';
import { ProductsListComponent } from './resources/pages/database/products-list/products-list.component';
import { EditProductComponent } from './resources/pages/database/edit-product/edit-product.component';
import { ReviewsListComponent } from './resources/pages/database/reviews-list/reviews-list.component';
import { EditReviewComponent } from './resources/pages/database/edit-review/edit-review.component';

const routes: Routes = [
    { path: '', component: EtlComponent },
    { path: 'database', component: DatabaseComponent, children: [
            { path: 'products', component: ProductsListComponent },
            { path: 'products/:id', component: EditProductComponent },
            { path: 'products/:id/reviews', component: ReviewsListComponent },
            { path: 'reviews/:id', component: EditReviewComponent },
        ]
    },

    { path: '**', redirectTo: '/' }
];

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})

export class AppRoutingModule {}
