export interface PaginationInterface
{
    results: Array<any>;
    pages: number;
    total: number;
}
