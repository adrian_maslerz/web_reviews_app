import { Model } from './model.model';
import { Attribute } from './attribute.model';
import { User } from './user.model';

export class Product extends Model
{
    id: string = null;
    name: string = null;
    created: number = null;
    link: string = null;
    image: string = null;
    reviews_count: number = 0;
    attributes: Array<Attribute> = [];

    constructor(object: Object = {})
    {
        super();
        this.assignProperties(this, object, {"id": "_id" });

        //sub objects
        object["attributes"] && Array.isArray(object["attributes"])? this.attributes = <Array<any>>object["attributes"].map(attribute => new Attribute(attribute)) : [];
    }
}
