import { Model } from './model.model';

export class User extends Model
{
    name: string = null;
    constructor(object: Object = {})
    {
        super();
        this.assignProperties(this, object);
    }
}
