import { Model } from './model.model';
import { User } from './user.model';

export class Review extends Model
{
    id: string = null;
    description: string = null;
    ups: number = 0;
    downs: number = 0;
    rate: number = 0;
    date: number = null;
    created: number = null;
    user: User = null;

    constructor(object: Object = {})
    {
        super();
        this.assignProperties(this, object, {"id": "_id" });

        //sub objects
        object["user"]? this.user = new User(object["user"]) : null;
    }
}
