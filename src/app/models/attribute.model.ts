import { Model } from './model.model';

export class Attribute extends Model
{
    name: string = null;
    values: Array<string> = [];

    constructor(object: Object = {})
    {
        super();
        this.assignProperties(this, object);

        //sub objects
        object["values"] && Array.isArray(object["values"])? this.values = <Array<string>>object["values"]: [];
    }
}
