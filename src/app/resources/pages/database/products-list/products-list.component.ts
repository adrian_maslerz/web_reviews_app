import { Component, OnInit } from '@angular/core';
import { Product } from '../../../../models/product.model';
import { ProductsDataService } from '../../../../services/data/products.data.service';
import { FormControl, FormGroup } from '@angular/forms';
import { PaginationInterface } from '../../../../interfaces/pagination.interface';
import { EtlDataService } from '../../../../services/data/etl.data.service';
import { environment } from '../../../../../environments/environment';

@Component({
    selector: 'app-products-list',
    templateUrl: './products-list.component.html',
    styleUrls: [ './products-list.component.scss' ],
    providers: [ ProductsDataService, EtlDataService ]
})
export class ProductsListComponent implements OnInit
{
    products: Array<Product> = [];
    page: number = 1;
    pages: number = 0;
    form: FormGroup;

    private api: string = environment.api + "/api";

    constructor(private productsDataService: ProductsDataService, private etlDataService: EtlDataService) { }

    ngOnInit()
    {
        //form handle
        this.form = new FormGroup({
            search: new FormControl(""),
            sort: new FormControl("nameAESC"),
            dateFrom: new FormControl(""),
            dateTo: new FormControl(""),
            reviewsFrom: new FormControl(""),
            reviewsTo: new FormControl(""),
        });

        this.form.valueChanges.subscribe(() => {
            this.page = 1;
            this.loadData()
        });

        this.loadData();
    }

    onPageChange(page: number)
    {
        this.page = page;
        this.loadData();
    }

    onChangeSort(sort: string)
    {
        this.form.get("sort").patchValue(sort);
    }

    loadData()
    {
        //getting data
        this.productsDataService
            .getProducts(
                this.page,
                this.form.get("search").value,
                this.form.get("sort").value,
                this.form.get("dateFrom").value,
                this.form.get("dateTo").value,
                this.form.get("reviewsFrom").value,
                this.form.get("reviewsTo").value,
            )
            .subscribe((response: PaginationInterface) => {

                this.pages = response.pages;
                this.products = <Array<Product>>response.results;
            })
    }

    onClearDatabase()
    {
        this.etlDataService.clearDatabase().subscribe(response => {
            console.log(response);
            this.loadData();
        })
    }

    onExportToCSV()
    {
        const link = this.api + "/products/export"
        const win = window.open(link, '_blank');
        win.focus();
    }

    onExportProductToTxt(id: string)
    {
        const link = this.api + "/products/" + id + "/export"
        const win = window.open(link, '_blank');
        win.focus();
    }

}
