import { Component, OnInit } from '@angular/core';
import { ReviewsDataService } from '../../../../services/data/reviews.data.service';
import { ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { Review } from '../../../../models/review.model';
import { PaginationInterface } from '../../../../interfaces/pagination.interface';
import { environment } from '../../../../../environments/environment';

@Component({
    selector: 'app-reviews-list',
    templateUrl: './reviews-list.component.html',
    styleUrls: [ './reviews-list.component.scss' ],
    providers: [ ReviewsDataService ]
})
export class ReviewsListComponent implements OnInit
{
    reviews: Array<Review> = [];
    page: number = 1;
    pages: number = 0;
    id: string;

    private api: string = environment.api + "/api";

    constructor(private reviewsDataService: ReviewsDataService, private route: ActivatedRoute) { }

    ngOnInit()
    {
        this.id = this.route.snapshot.params["id"];
        this.loadData();
    }

    onPageChange(page: number)
    {
        this.page = page;
        this.loadData();
    }

    loadData()
    {
        //getting data
        this.reviewsDataService
            .getProductReviews(
                this.id,
                this.page,
            )
            .subscribe((response: PaginationInterface) => {

                this.pages = response.pages;
                this.reviews = <Array<Review>>response.results;
            })
    }

    onExportReviewToTxt(id: string)
    {
        const link = this.api + "/reviews/" + id + "/export"
        const win = window.open(link, '_blank');
        win.focus();
    }
}
