import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { getMessageError, handleValidationErrorMessage, handleValidationStateClass } from '../../../../utilities/form.utils';
import { ProductsDataService } from '../../../../services/data/products.data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from '../../../../models/product.model';
import { fileSize, fileType } from '../../../../utilities/validators';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
    selector: 'app-edit-product',
    templateUrl: './edit-product.component.html',
    styleUrls: [ './edit-product.component.scss' ],
    providers: [ ProductsDataService ]
})
export class EditProductComponent implements OnInit
{
    id: string;
    product: Product;

    form: FormGroup;
    formUtils = { handleValidationStateClass, handleValidationErrorMessage }
    inProgress: boolean = false;
    error: string = '';

    file: File;
    reader: FileReader = new FileReader();
    private maxFileSizeMB = 5;
    private validMimeTypes = [
        "image/png",
        "image/jpg",
        "image/jpeg"
    ];

    messages =  [
        {

            field: 'name',
            errors: [
                {
                    error: 'required',
                    message: 'Name is required'
                }
            ]
        },
        {

            field: 'image',
            errors: [
                {
                    error: 'fileType',
                    message: 'Invalid file type'
                },
                {
                    error: 'fileSize',
                    message: 'Your image cannot be larger than 5MB'
                },
            ]
        }
    ];

    attributeFormMessages = [
        {
            field: 'name',
            errors: [
                {
                    error: 'required',
                    message: 'Attribute name is required'
                }
            ]
        }
    ];

    valueFormMessages = [
        {
            field: 'value',
            errors: [
                {
                    error: 'required',
                    message: 'Attribute value is required'
                }
            ]
        }
    ]

    constructor(private productsDataService: ProductsDataService,  private route: ActivatedRoute, private router: Router) { }


    ngOnInit()
    {
        //getting id
        this.id = this.route.snapshot.params["id"];

        //building form
        this.form = new FormGroup({
            name: new FormControl(null, [ Validators.required ]),
            image: new FormControl(null),
            attributes: new FormArray([])
        });

        this.getProduct()

        //reader
        this.reader.onload = (event) => {
            this.product.image = event.target["result"];
        }
    }

    onSubmit()
    {
        if(this.form.valid)
        {
            this.inProgress = true;
            this.productsDataService.updateProduct(this.id, this.form.value , this.file).subscribe(() => {
                    this.inProgress = false;
                    this.router.navigate(["database",'products']);
                },
                (error: HttpErrorResponse) => {
                    this.error = getMessageError(error);
                    this.inProgress = false;
                });
        }
    }

    getProduct()
    {
        this.productsDataService.getProduct(this.id).subscribe((product: Product) => {
            this.product = product;

            //patching form values
            this.form.reset();
            this.form.patchValue({
                name: product.name
            });

            product.attributes.forEach(attribute => {
                (<FormArray>this.form.get("attributes")).push(new FormGroup({
                    name: new FormControl(attribute.name, Validators.required),
                    values: new FormArray(attribute.values.map(value => new FormGroup({
                        value: new FormControl(value, Validators.required)
                    })))
                }))
            })

        })
    }

    //attribute buttons handle
    onAddAttribute(): void
    {
        (<FormArray>this.form.get('attributes')).push(
            new FormGroup({
                name: new FormControl(null, Validators.required),
                values: new FormArray([
                    new FormGroup({
                        value: new FormControl(null, Validators.required)
                    })
                ])
            })
        );
    }

    onAddAttributeValue(index: number): void
    {
        (<FormArray>this.form.get('attributes')["controls"][index]["controls"]["values"]).push(new FormGroup({
            value: new FormControl(null, Validators.required)
        }));
    }

    onRemoveAttribute(index: number): void
    {
        (<FormArray>this.form.get('attributes')).removeAt(index);
    }

    onRemoveAttributeValue(attributeIndex: number, valueIndex: number): void
    {
        if((<FormArray>this.form.get('attributes')["controls"][attributeIndex]["controls"]["values"]).length > 1)
            (<FormArray>this.form.get('attributes')["controls"][attributeIndex]["controls"]["values"]).removeAt(valueIndex);
    }

    //file handle
    onFileChange(event)
    {
        if (event.target.files.length)
        {
            const fileResource = event.target.files[0];
            if (this.validMimeTypes.indexOf(fileResource.type) !== -1 && fileResource.size < 1024 * 1024 * this.maxFileSizeMB)
            {
                this.file = fileResource;
                this.reader.readAsDataURL(fileResource);
            }

            const control = this.form.get('image');
            control.setValue(fileResource.name);
            control.markAsTouched();
            control.setValidators([ fileType(fileResource, this.validMimeTypes), fileSize(fileResource, this.maxFileSizeMB)]);
            control.updateValueAndValidity();
        }
    }

}
