import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ProductsDataService } from '../../../../services/data/products.data.service';
import { getMessageError, handleValidationErrorMessage, handleValidationStateClass } from '../../../../utilities/form.utils';
import { Review } from '../../../../models/review.model';
import { ActivatedRoute, Router } from '@angular/router';
import { ReviewsDataService } from '../../../../services/data/reviews.data.service';
import { invalidDate } from '../../../../utilities/validators';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
    selector: 'app-edit-review',
    templateUrl: './edit-review.component.html',
    styleUrls: [ './edit-review.component.scss' ],
    providers: [ ReviewsDataService ]
})
export class EditReviewComponent implements OnInit
{
    id: string;
    review: Review;

    form: FormGroup;
    formUtils = { handleValidationStateClass, handleValidationErrorMessage };
    inProgress: boolean = false;
    error: string = '';

    messages = [
        {
            field: 'description',
            errors: [
                {
                    error: 'required',
                    message: 'Description is required'
                }
            ]
        },
        {
            field: 'user',
            errors: [
                {
                    error: 'required',
                    message: 'User is required'
                }
            ]
        },
        {
            field: 'ups',
            errors: [
                {
                    error: 'required',
                    message: 'Ups is required'
                },
                {
                    error: 'min',
                    message: 'Ups cannot be lower than 0'
                }
            ]
        },
        {
            field: 'downs',
            errors: [
                {
                    error: 'required',
                    message: 'Downs is required'
                },
                {
                    error: 'min',
                    message: 'Downs cannot be lower than 0'
                }
            ]
        },
        {
            field: 'rate',
            errors: [
                {
                    error: 'required',
                    message: 'Rate is required'
                },
                {
                    error: 'min',
                    message: 'Rate cannot be lower than 0'
                },
                {
                    error: 'max',
                    message: 'Rate cannot higher than 6'
                }
            ]
        },
        {
            field: 'date',
            errors: [
                {
                    error: 'required',
                    message: 'Date is required'
                },
                {
                    error: 'invalidDate',
                    message: 'Invalid date'
                }
            ]
        }
    ];

    constructor(private reviewsDataService: ReviewsDataService, private route: ActivatedRoute, private router: Router) { }


    ngOnInit()
    {
        //getting id
        this.id = this.route.snapshot.params[ 'id' ];

        //building form
        this.form = new FormGroup({
            description: new FormControl(null, [Validators.required]),
            ups: new FormControl(null, [Validators.required, Validators.min(0)]),
            downs: new FormControl(null, [Validators.required, Validators.min(0)]),
            rate: new FormControl(null, [Validators.required, Validators.min(0), Validators.max(6)]),
            date: new FormControl(null, [Validators.required, invalidDate]),
            user: new FormControl(null, [Validators.required])
        });

        this.getReview();
    }

    onSubmit()
    {
        if (this.form.valid)
        {
            this.reviewsDataService.updateReview(this.id, this.form.value).subscribe(() => {
                    this.inProgress = false;
                    this.router.navigate(["database",'products']);
                },
                (error: HttpErrorResponse) => {
                    this.error = getMessageError(error);
                    this.inProgress = false;
                });
        }
    }

    getReview()
    {
        this.reviewsDataService.getReview(this.id).subscribe((review: Review) =>
        {
            this.review = review;

            //patching form values
            this.form.reset();
            this.form.patchValue({
                description: review.description,
                ups: review.ups,
                downs: review.downs,
                rate: review.rate,
                date: new Date(review.date),
                user: review.user.name,
            });
        });
    }

}
