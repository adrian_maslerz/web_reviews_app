import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { EtlDataService } from '../../../services/data/etl.data.service';
import { EtlStateService } from '../../../services/state/etl.state.service';
import { handleValidationErrorMessage, handleValidationStateClass } from '../../../utilities/form.utils';

@Component({
    selector: 'app-etl',
    templateUrl: './etl.component.html',
    styleUrls: [ './etl.component.scss' ],
    providers: [ EtlDataService ]
})
export class EtlComponent implements OnInit
{
    form: FormGroup;
    formUtils = { handleValidationStateClass, handleValidationErrorMessage }

    messages = [
        {
            field: 'search',
            errors: [
                {
                    error: 'required',
                    message: 'Search is required'
                },
                {
                    error: 'minlength',
                    message: 'Searched phrase must have at least 4 characters'
                }
            ]
        }
    ];

    constructor(private etlDataService: EtlDataService, public etlStateService: EtlStateService) { }

    ngOnInit()
    {
        this.form = new FormGroup({
            search: new FormControl("", [Validators.required, Validators.minLength(4)])
        });
    }

    //full etl
    onSubmit()
    {
        if(this.form.valid)
        {
            this.etlDataService.etl(this.form.get("search").value).subscribe(data => {

            })
        }
    }

    //extract
    onExtract()
    {
        if(this.form.valid)
        {
            this.etlDataService.extract(this.form.get("search").value).subscribe(data => {

            })
        }
    }

    //transform
    onTransform()
    {
        this.etlDataService.transform().subscribe(data => {
            console.log(data);
        })
    }

    //transform
    onLoad()
    {
        this.etlDataService.load().subscribe(data => {
            console.log(data);
        })
    }
}
